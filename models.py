from app import db
from datetime import datetime
import re


# получаем строку ЧПУ для URL
def slugify(s):
    pattern = r'[^\w+]'
    return re.sub(pattern, '-', s)

# Связь "посты-тэги" мгоие ко многим
post_tags = db.Table('post_tags',
                     db.Column('post_id', db.Integer, db.ForeignKey('post.id')),  # id поста
                     db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'))  # id тэга
                     )

# БД постов
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # подключаем ЧПУ
    title = db.Column(db.String(140))
    slug = db.Column(db.String(140), unique=True)
    body = db.Column(db.Text)
    created = db.Column(db.DateTime, default=datetime.now())

    # получаем аргументы которые передали на вход конструктора
    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)
        # получаем URL от функции
        self.generate_slug()

    # функция генерации URL из заголовка для ЧПУ
    def generate_slug(self):
        if self.title:
            self.slug = slugify(self.title)

    # формируем удобочитаемую строку с данными
    def __repr__(self):
        return '<Post: id: {}, title: {}>'.format(self.id, self.title)

    tags = db.relationship('Tag', secondary=post_tags, backref=db.backref('posts', lazy='dynamic'))


# класс для обработки тэгов
class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))  # название тэга
    slug = db.Column(db.String(100))  # URL ЧПУ для вывода результата

    def __init__(self, *args, **kwargs):  # получаем аргуементы, которые ищем списком
        super(Tag, self).__init__(*args, **kwargs)
        self.slug = slugify(self.name)  # формируем ЧПУ для них

    def __repr__(self):  # формируем удобочитаемую строку с данными
        return '<Tag id: {}, name: {}>'.format(self.id, self.name)
