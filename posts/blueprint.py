from flask import Blueprint
from flask import render_template

from models import Post

posts = Blueprint('posts', __name__, template_folder='templates')


# выводим список постов
# localhost/blog/
@posts.route('/')
def index():
    blog_posts = Post.query.all()
    return render_template('posts/index.html', posts=blog_posts)


# выводим детальную карточку постов
# localhost/blog/name_post
@posts.route('/<slug>')
def post_detail(slug):
    post = Post.query.filter(Post.slug == slug).first()  # это временная мера, тут нужна проверка уникальности
    return render_template('posts/post_detail.html', post=post)

