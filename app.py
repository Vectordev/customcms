from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from config import Config

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager


# обращаемся к приложению
app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


@app.route('/')
def index():
    return render_template('index.html')









