from app import app
from app import db
from posts.blueprint import posts  # blueprint постов

app.register_blueprint(posts, url_prefix='/blog')  # посты в папке "posts" с адресом "/blog"

# запускаем приложение
# делаем это тут, потому что если сделать это в "app.pv"
# то будет круговое импортирование между "blueprint" и "app"
#  из-за класса "import Post" который в "blueprint" подключается
if __name__ == '__main__':
    app.run()

